﻿namespace SIGMA.TelasMenorde40
{
    partial class CadrastrodeUsuariosdoSistema
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.lblcodigo = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtbconfirmarsenha = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.cbxdepartamento = new System.Windows.Forms.ComboBox();
            this.txtbnome = new System.Windows.Forms.TextBox();
            this.txtbusuario = new System.Windows.Forms.TextBox();
            this.txtbconfirmar = new System.Windows.Forms.TextBox();
            this.txtbsenha = new System.Windows.Forms.TextBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.rbtsim = new System.Windows.Forms.RadioButton();
            this.rbtnao = new System.Windows.Forms.RadioButton();
            this.label7 = new System.Windows.Forms.Label();
            this.lbxobsevação = new System.Windows.Forms.ListBox();
            this.btfechar = new System.Windows.Forms.Button();
            this.btsalvar = new System.Windows.Forms.Button();
            this.btexcluir = new System.Windows.Forms.Button();
            this.btcancelar = new System.Windows.Forms.Button();
            this.btalterar = new System.Windows.Forms.Button();
            this.btinserir = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(49, 41);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Código:";
            // 
            // lblcodigo
            // 
            this.lblcodigo.AutoSize = true;
            this.lblcodigo.Location = new System.Drawing.Point(98, 41);
            this.lblcodigo.Name = "lblcodigo";
            this.lblcodigo.Size = new System.Drawing.Size(13, 13);
            this.lblcodigo.TabIndex = 1;
            this.lblcodigo.Text = "0";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(57, 77);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(38, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Nome:";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(54, 186);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(41, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Senha:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(58, 150);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(33, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Perfil:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(45, 113);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(46, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "Usúario:";
            // 
            // txtbconfirmarsenha
            // 
            this.txtbconfirmarsenha.AutoSize = true;
            this.txtbconfirmarsenha.Location = new System.Drawing.Point(9, 221);
            this.txtbconfirmarsenha.Name = "txtbconfirmarsenha";
            this.txtbconfirmarsenha.Size = new System.Drawing.Size(86, 13);
            this.txtbconfirmarsenha.TabIndex = 6;
            this.txtbconfirmarsenha.Text = "Confirmar senha:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(220, 41);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(77, 13);
            this.label8.TabIndex = 7;
            this.label8.Text = "Departamento:";
            // 
            // cbxdepartamento
            // 
            this.cbxdepartamento.FormattingEnabled = true;
            this.cbxdepartamento.Location = new System.Drawing.Point(303, 38);
            this.cbxdepartamento.Name = "cbxdepartamento";
            this.cbxdepartamento.Size = new System.Drawing.Size(154, 21);
            this.cbxdepartamento.TabIndex = 8;
            // 
            // txtbnome
            // 
            this.txtbnome.Location = new System.Drawing.Point(101, 74);
            this.txtbnome.Name = "txtbnome";
            this.txtbnome.Size = new System.Drawing.Size(356, 20);
            this.txtbnome.TabIndex = 9;
            // 
            // txtbusuario
            // 
            this.txtbusuario.Location = new System.Drawing.Point(101, 110);
            this.txtbusuario.Name = "txtbusuario";
            this.txtbusuario.Size = new System.Drawing.Size(356, 20);
            this.txtbusuario.TabIndex = 10;
            // 
            // txtbconfirmar
            // 
            this.txtbconfirmar.Location = new System.Drawing.Point(101, 218);
            this.txtbconfirmar.Name = "txtbconfirmar";
            this.txtbconfirmar.Size = new System.Drawing.Size(196, 20);
            this.txtbconfirmar.TabIndex = 12;
            // 
            // txtbsenha
            // 
            this.txtbsenha.Location = new System.Drawing.Point(101, 183);
            this.txtbsenha.Name = "txtbsenha";
            this.txtbsenha.Size = new System.Drawing.Size(196, 20);
            this.txtbsenha.TabIndex = 13;
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(101, 147);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(356, 21);
            this.comboBox1.TabIndex = 14;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(334, 186);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(85, 13);
            this.label2.TabIndex = 15;
            this.label2.Text = "Autorizar acesso";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // rbtsim
            // 
            this.rbtsim.AutoSize = true;
            this.rbtsim.Location = new System.Drawing.Point(337, 202);
            this.rbtsim.Name = "rbtsim";
            this.rbtsim.Size = new System.Drawing.Size(42, 17);
            this.rbtsim.TabIndex = 16;
            this.rbtsim.TabStop = true;
            this.rbtsim.Text = "Sim";
            this.rbtsim.UseVisualStyleBackColor = true;
            // 
            // rbtnao
            // 
            this.rbtnao.AutoSize = true;
            this.rbtnao.Location = new System.Drawing.Point(337, 219);
            this.rbtnao.Name = "rbtnao";
            this.rbtnao.Size = new System.Drawing.Size(45, 17);
            this.rbtnao.TabIndex = 17;
            this.rbtnao.TabStop = true;
            this.rbtnao.Text = "Não";
            this.rbtnao.UseVisualStyleBackColor = true;
            this.rbtnao.CheckedChanged += new System.EventHandler(this.rbtnao_CheckedChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(24, 268);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(68, 13);
            this.label7.TabIndex = 18;
            this.label7.Text = "Observação:";
            // 
            // lbxobsevação
            // 
            this.lbxobsevação.FormattingEnabled = true;
            this.lbxobsevação.Location = new System.Drawing.Point(23, 284);
            this.lbxobsevação.Name = "lbxobsevação";
            this.lbxobsevação.Size = new System.Drawing.Size(480, 82);
            this.lbxobsevação.TabIndex = 19;
            // 
            // btfechar
            // 
            this.btfechar.Location = new System.Drawing.Point(433, 428);
            this.btfechar.Name = "btfechar";
            this.btfechar.Size = new System.Drawing.Size(88, 31);
            this.btfechar.TabIndex = 20;
            this.btfechar.Text = "Fechar";
            this.btfechar.UseVisualStyleBackColor = true;
            // 
            // btsalvar
            // 
            this.btsalvar.Location = new System.Drawing.Point(347, 428);
            this.btsalvar.Name = "btsalvar";
            this.btsalvar.Size = new System.Drawing.Size(88, 31);
            this.btsalvar.TabIndex = 21;
            this.btsalvar.Text = "Salvar";
            this.btsalvar.UseVisualStyleBackColor = true;
            // 
            // btexcluir
            // 
            this.btexcluir.Location = new System.Drawing.Point(261, 428);
            this.btexcluir.Name = "btexcluir";
            this.btexcluir.Size = new System.Drawing.Size(88, 31);
            this.btexcluir.TabIndex = 22;
            this.btexcluir.Text = "Excluir";
            this.btexcluir.UseVisualStyleBackColor = true;
            // 
            // btcancelar
            // 
            this.btcancelar.Location = new System.Drawing.Point(175, 428);
            this.btcancelar.Name = "btcancelar";
            this.btcancelar.Size = new System.Drawing.Size(88, 31);
            this.btcancelar.TabIndex = 23;
            this.btcancelar.Text = "Cancelar";
            this.btcancelar.UseVisualStyleBackColor = true;
            // 
            // btalterar
            // 
            this.btalterar.Location = new System.Drawing.Point(89, 428);
            this.btalterar.Name = "btalterar";
            this.btalterar.Size = new System.Drawing.Size(88, 31);
            this.btalterar.TabIndex = 24;
            this.btalterar.Text = "Alterar";
            this.btalterar.UseVisualStyleBackColor = true;
            // 
            // btinserir
            // 
            this.btinserir.Location = new System.Drawing.Point(3, 428);
            this.btinserir.Name = "btinserir";
            this.btinserir.Size = new System.Drawing.Size(88, 31);
            this.btinserir.TabIndex = 25;
            this.btinserir.Text = "Inserir";
            this.btinserir.UseVisualStyleBackColor = true;
            // 
            // CadrastrodeUsuariosdoSistema
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.ClientSize = new System.Drawing.Size(524, 463);
            this.Controls.Add(this.btinserir);
            this.Controls.Add(this.btalterar);
            this.Controls.Add(this.btcancelar);
            this.Controls.Add(this.btexcluir);
            this.Controls.Add(this.btsalvar);
            this.Controls.Add(this.btfechar);
            this.Controls.Add(this.lbxobsevação);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.rbtnao);
            this.Controls.Add(this.rbtsim);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.txtbsenha);
            this.Controls.Add(this.txtbconfirmar);
            this.Controls.Add(this.txtbusuario);
            this.Controls.Add(this.txtbnome);
            this.Controls.Add(this.cbxdepartamento);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txtbconfirmarsenha);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lblcodigo);
            this.Controls.Add(this.label1);
            this.Name = "CadrastrodeUsuariosdoSistema";
            this.Text = "Cadrastro de Usuarios do Sistema";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblcodigo;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label txtbconfirmarsenha;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox cbxdepartamento;
        private System.Windows.Forms.TextBox txtbnome;
        private System.Windows.Forms.TextBox txtbusuario;
        private System.Windows.Forms.TextBox txtbconfirmar;
        private System.Windows.Forms.TextBox txtbsenha;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RadioButton rbtsim;
        private System.Windows.Forms.RadioButton rbtnao;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ListBox lbxobsevação;
        private System.Windows.Forms.Button btfechar;
        private System.Windows.Forms.Button btsalvar;
        private System.Windows.Forms.Button btexcluir;
        private System.Windows.Forms.Button btcancelar;
        private System.Windows.Forms.Button btalterar;
        private System.Windows.Forms.Button btinserir;
    }
}