﻿namespace WindowsFormsApp4
{
    partial class Cadrastro_de_Clientes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.mtxtbclientedesde = new System.Windows.Forms.MaskedTextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.mtxtbdatadenacimento = new System.Windows.Forms.MaskedTextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.radioButton3 = new System.Windows.Forms.RadioButton();
            this.radioButton4 = new System.Windows.Forms.RadioButton();
            this.label6 = new System.Windows.Forms.Label();
            this.txtbprofisssao = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.mtxtbCPF = new System.Windows.Forms.MaskedTextBox();
            this.txtbnome = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.txbrua = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.txbbairro = new System.Windows.Forms.TextBox();
            this.txbcidade = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txbreferencia = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.txtbnumero = new System.Windows.Forms.TextBox();
            this.mtxtbCEP = new System.Windows.Forms.MaskedTextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.cbxestado = new System.Windows.Forms.ComboBox();
            this.txtbcomplemento = new System.Windows.Forms.TextBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.mtxbtelefone = new System.Windows.Forms.MaskedTextBox();
            this.mtxbcelular = new System.Windows.Forms.MaskedTextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.txbemail = new System.Windows.Forms.TextBox();
            this.mtxbtelefonecomercial = new System.Windows.Forms.MaskedTextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.txbramal = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.btfechar = new System.Windows.Forms.Button();
            this.btSalvar = new System.Windows.Forms.Button();
            this.btinserir = new System.Windows.Forms.Button();
            this.btalterar = new System.Windows.Forms.Button();
            this.btcancelar = new System.Windows.Forms.Button();
            this.btexcluir = new System.Windows.Forms.Button();
            this.localizarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.imprimirToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.observaçõesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.groupBox1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.mtxtbclientedesde);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.mtxtbdatadenacimento);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.groupBox3);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.txtbprofisssao);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.mtxtbCPF);
            this.groupBox1.Controls.Add(this.txtbnome);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(12, 44);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(664, 138);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Cliente";
            // 
            // mtxtbclientedesde
            // 
            this.mtxtbclientedesde.Location = new System.Drawing.Point(558, 107);
            this.mtxtbclientedesde.Mask = "00/00/0000";
            this.mtxtbclientedesde.Name = "mtxtbclientedesde";
            this.mtxtbclientedesde.Size = new System.Drawing.Size(69, 20);
            this.mtxtbclientedesde.TabIndex = 14;
            this.mtxtbclientedesde.ValidatingType = typeof(System.DateTime);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(478, 110);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(74, 13);
            this.label8.TabIndex = 13;
            this.label8.Text = "Cliente desde:";
            // 
            // mtxtbdatadenacimento
            // 
            this.mtxtbdatadenacimento.Location = new System.Drawing.Point(435, 72);
            this.mtxtbdatadenacimento.Mask = "00/00/0000";
            this.mtxtbdatadenacimento.Name = "mtxtbdatadenacimento";
            this.mtxtbdatadenacimento.Size = new System.Drawing.Size(69, 20);
            this.mtxtbdatadenacimento.TabIndex = 12;
            this.mtxtbdatadenacimento.ValidatingType = typeof(System.DateTime);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(510, 75);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(69, 13);
            this.label7.TabIndex = 11;
            this.label7.Text = "dd/mm/aaaa";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.radioButton3);
            this.groupBox3.Controls.Add(this.radioButton4);
            this.groupBox3.Location = new System.Drawing.Point(435, 9);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(200, 31);
            this.groupBox3.TabIndex = 10;
            this.groupBox3.TabStop = false;
            // 
            // radioButton3
            // 
            this.radioButton3.AutoSize = true;
            this.radioButton3.Location = new System.Drawing.Point(99, 10);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new System.Drawing.Size(99, 17);
            this.radioButton3.TabIndex = 1;
            this.radioButton3.TabStop = true;
            this.radioButton3.Text = "Pessoa Juridica";
            this.radioButton3.UseVisualStyleBackColor = true;
            // 
            // radioButton4
            // 
            this.radioButton4.AutoSize = true;
            this.radioButton4.Location = new System.Drawing.Point(6, 12);
            this.radioButton4.Name = "radioButton4";
            this.radioButton4.Size = new System.Drawing.Size(87, 17);
            this.radioButton4.TabIndex = 0;
            this.radioButton4.TabStop = true;
            this.radioButton4.Text = "Pessoa fisica";
            this.radioButton4.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(10, 21);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(43, 13);
            this.label6.TabIndex = 9;
            this.label6.Text = "Codigo:";
            // 
            // txtbprofisssao
            // 
            this.txtbprofisssao.Location = new System.Drawing.Point(435, 46);
            this.txtbprofisssao.Name = "txtbprofisssao";
            this.txtbprofisssao.Size = new System.Drawing.Size(200, 20);
            this.txtbprofisssao.TabIndex = 8;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(327, 75);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(102, 13);
            this.label5.TabIndex = 7;
            this.label5.Text = "Data de Nacimento:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(376, 49);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Profissão:";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // mtxtbCPF
            // 
            this.mtxtbCPF.Location = new System.Drawing.Point(57, 72);
            this.mtxtbCPF.Mask = "XXX,XXX,XXX-XX";
            this.mtxtbCPF.Name = "mtxtbCPF";
            this.mtxtbCPF.Size = new System.Drawing.Size(96, 20);
            this.mtxtbCPF.TabIndex = 5;
            // 
            // txtbnome
            // 
            this.txtbnome.Location = new System.Drawing.Point(57, 46);
            this.txtbnome.Name = "txtbnome";
            this.txtbnome.Size = new System.Drawing.Size(313, 20);
            this.txtbnome.TabIndex = 4;
            this.txtbnome.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(19, 110);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(34, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Sexo:";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.radioButton2);
            this.groupBox2.Controls.Add(this.radioButton1);
            this.groupBox2.Location = new System.Drawing.Point(57, 96);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(161, 36);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(84, 12);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(67, 17);
            this.radioButton2.TabIndex = 1;
            this.radioButton2.TabStop = true;
            this.radioButton2.Text = "Feminino";
            this.radioButton2.UseVisualStyleBackColor = true;
            this.radioButton2.CheckedChanged += new System.EventHandler(this.radioButton2_CheckedChanged);
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Location = new System.Drawing.Point(6, 12);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(73, 17);
            this.radioButton1.TabIndex = 0;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "Masculino";
            this.radioButton1.UseVisualStyleBackColor = true;
            this.radioButton1.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(23, 75);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(30, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "CFP:";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 49);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nome:";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.txtbcomplemento);
            this.groupBox4.Controls.Add(this.cbxestado);
            this.groupBox4.Controls.Add(this.label16);
            this.groupBox4.Controls.Add(this.label15);
            this.groupBox4.Controls.Add(this.mtxtbCEP);
            this.groupBox4.Controls.Add(this.txtbnumero);
            this.groupBox4.Controls.Add(this.label14);
            this.groupBox4.Controls.Add(this.label13);
            this.groupBox4.Controls.Add(this.label12);
            this.groupBox4.Controls.Add(this.txbreferencia);
            this.groupBox4.Controls.Add(this.label11);
            this.groupBox4.Controls.Add(this.txbcidade);
            this.groupBox4.Controls.Add(this.txbbairro);
            this.groupBox4.Controls.Add(this.label10);
            this.groupBox4.Controls.Add(this.label9);
            this.groupBox4.Controls.Add(this.txbrua);
            this.groupBox4.Location = new System.Drawing.Point(12, 188);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(664, 109);
            this.groupBox4.TabIndex = 2;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Endereço";
            // 
            // txbrua
            // 
            this.txbrua.Location = new System.Drawing.Point(59, 19);
            this.txbrua.Name = "txbrua";
            this.txbrua.Size = new System.Drawing.Size(236, 20);
            this.txbrua.TabIndex = 0;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(23, 22);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(30, 13);
            this.label9.TabIndex = 1;
            this.label9.Text = "Rua:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(16, 48);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(37, 13);
            this.label10.TabIndex = 2;
            this.label10.Text = "Bairro:";
            // 
            // txbbairro
            // 
            this.txbbairro.Location = new System.Drawing.Point(59, 45);
            this.txbbairro.Name = "txbbairro";
            this.txbbairro.Size = new System.Drawing.Size(236, 20);
            this.txbbairro.TabIndex = 3;
            // 
            // txbcidade
            // 
            this.txbcidade.Location = new System.Drawing.Point(59, 71);
            this.txbcidade.Name = "txbcidade";
            this.txbcidade.Size = new System.Drawing.Size(235, 20);
            this.txbcidade.TabIndex = 4;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(12, 74);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(40, 13);
            this.label11.TabIndex = 5;
            this.label11.Text = "Cidade";
            // 
            // txbreferencia
            // 
            this.txbreferencia.Location = new System.Drawing.Point(372, 71);
            this.txbreferencia.Name = "txbreferencia";
            this.txbreferencia.Size = new System.Drawing.Size(284, 20);
            this.txbreferencia.TabIndex = 6;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(304, 74);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(62, 13);
            this.label12.TabIndex = 7;
            this.label12.Text = "Referência:";
            this.label12.Click += new System.EventHandler(this.label12_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(319, 22);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(47, 13);
            this.label13.TabIndex = 8;
            this.label13.Text = "Número:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(335, 47);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(31, 13);
            this.label14.TabIndex = 9;
            this.label14.Text = "CEP:";
            this.label14.Click += new System.EventHandler(this.label14_Click);
            // 
            // txtbnumero
            // 
            this.txtbnumero.Location = new System.Drawing.Point(372, 19);
            this.txtbnumero.Name = "txtbnumero";
            this.txtbnumero.Size = new System.Drawing.Size(100, 20);
            this.txtbnumero.TabIndex = 10;
            // 
            // mtxtbCEP
            // 
            this.mtxtbCEP.Location = new System.Drawing.Point(372, 45);
            this.mtxtbCEP.Name = "mtxtbCEP";
            this.mtxtbCEP.Size = new System.Drawing.Size(100, 20);
            this.mtxtbCEP.TabIndex = 11;
            this.mtxtbCEP.MaskInputRejected += new System.Windows.Forms.MaskInputRejectedEventHandler(this.maskedTextBox4_MaskInputRejected);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(478, 22);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(74, 13);
            this.label15.TabIndex = 12;
            this.label15.Text = "Complemento:";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(509, 48);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(43, 13);
            this.label16.TabIndex = 13;
            this.label16.Text = "Estado:";
            // 
            // cbxestado
            // 
            this.cbxestado.FormattingEnabled = true;
            this.cbxestado.Location = new System.Drawing.Point(555, 44);
            this.cbxestado.Name = "cbxestado";
            this.cbxestado.Size = new System.Drawing.Size(101, 21);
            this.cbxestado.TabIndex = 14;
            // 
            // txtbcomplemento
            // 
            this.txtbcomplemento.Location = new System.Drawing.Point(555, 19);
            this.txtbcomplemento.Name = "txtbcomplemento";
            this.txtbcomplemento.Size = new System.Drawing.Size(100, 20);
            this.txtbcomplemento.TabIndex = 15;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.label22);
            this.groupBox5.Controls.Add(this.txbramal);
            this.groupBox5.Controls.Add(this.label21);
            this.groupBox5.Controls.Add(this.mtxbtelefonecomercial);
            this.groupBox5.Controls.Add(this.txbemail);
            this.groupBox5.Controls.Add(this.label20);
            this.groupBox5.Controls.Add(this.label19);
            this.groupBox5.Controls.Add(this.mtxbcelular);
            this.groupBox5.Controls.Add(this.mtxbtelefone);
            this.groupBox5.Controls.Add(this.label18);
            this.groupBox5.Controls.Add(this.label17);
            this.groupBox5.Location = new System.Drawing.Point(15, 303);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(661, 84);
            this.groupBox5.TabIndex = 3;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Contato";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(9, 26);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(52, 13);
            this.label17.TabIndex = 0;
            this.label17.Text = "Telefone:";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(20, 52);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(42, 13);
            this.label18.TabIndex = 1;
            this.label18.Text = "Celular:";
            // 
            // mtxbtelefone
            // 
            this.mtxbtelefone.Location = new System.Drawing.Point(67, 23);
            this.mtxbtelefone.Mask = "(99) 000-0000";
            this.mtxbtelefone.Name = "mtxbtelefone";
            this.mtxbtelefone.Size = new System.Drawing.Size(100, 20);
            this.mtxbtelefone.TabIndex = 2;
            // 
            // mtxbcelular
            // 
            this.mtxbcelular.Location = new System.Drawing.Point(68, 49);
            this.mtxbcelular.Mask = "(99) 000-0000";
            this.mtxbcelular.Name = "mtxbcelular";
            this.mtxbcelular.Size = new System.Drawing.Size(100, 20);
            this.mtxbcelular.TabIndex = 3;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(184, 26);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(100, 13);
            this.label19.TabIndex = 4;
            this.label19.Text = "Telefone comercial:";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(184, 52);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(35, 13);
            this.label20.TabIndex = 5;
            this.label20.Text = "Email:";
            // 
            // txbemail
            // 
            this.txbemail.Location = new System.Drawing.Point(225, 49);
            this.txbemail.Name = "txbemail";
            this.txbemail.Size = new System.Drawing.Size(276, 20);
            this.txbemail.TabIndex = 6;
            // 
            // mtxbtelefonecomercial
            // 
            this.mtxbtelefonecomercial.Location = new System.Drawing.Point(290, 23);
            this.mtxbtelefonecomercial.Mask = "(99) 000-0000";
            this.mtxbtelefonecomercial.Name = "mtxbtelefonecomercial";
            this.mtxbtelefonecomercial.Size = new System.Drawing.Size(100, 20);
            this.mtxbtelefonecomercial.TabIndex = 7;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(410, 26);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(40, 13);
            this.label21.TabIndex = 8;
            this.label21.Text = "Ramal:";
            // 
            // txbramal
            // 
            this.txbramal.Location = new System.Drawing.Point(456, 23);
            this.txbramal.Name = "txbramal";
            this.txbramal.Size = new System.Drawing.Size(100, 20);
            this.txbramal.TabIndex = 9;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(515, 52);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(123, 13);
            this.label22.TabIndex = 10;
            this.label22.Text = "Ex. oficina@hotmail.com";
            // 
            // btfechar
            // 
            this.btfechar.Location = new System.Drawing.Point(601, 421);
            this.btfechar.Name = "btfechar";
            this.btfechar.Size = new System.Drawing.Size(75, 32);
            this.btfechar.TabIndex = 4;
            this.btfechar.Text = "Fechar";
            this.btfechar.UseVisualStyleBackColor = true;
            // 
            // btSalvar
            // 
            this.btSalvar.Location = new System.Drawing.Point(520, 421);
            this.btSalvar.Name = "btSalvar";
            this.btSalvar.Size = new System.Drawing.Size(75, 32);
            this.btSalvar.TabIndex = 5;
            this.btSalvar.Text = "Salvar";
            this.btSalvar.UseVisualStyleBackColor = true;
            // 
            // btinserir
            // 
            this.btinserir.Location = new System.Drawing.Point(12, 421);
            this.btinserir.Name = "btinserir";
            this.btinserir.Size = new System.Drawing.Size(75, 32);
            this.btinserir.TabIndex = 6;
            this.btinserir.Text = "Inserir";
            this.btinserir.UseVisualStyleBackColor = true;
            // 
            // btalterar
            // 
            this.btalterar.Location = new System.Drawing.Point(93, 421);
            this.btalterar.Name = "btalterar";
            this.btalterar.Size = new System.Drawing.Size(75, 32);
            this.btalterar.TabIndex = 7;
            this.btalterar.Text = "Alterar";
            this.btalterar.UseVisualStyleBackColor = true;
            // 
            // btcancelar
            // 
            this.btcancelar.Location = new System.Drawing.Point(174, 421);
            this.btcancelar.Name = "btcancelar";
            this.btcancelar.Size = new System.Drawing.Size(75, 32);
            this.btcancelar.TabIndex = 8;
            this.btcancelar.Text = "Cancelar";
            this.btcancelar.UseVisualStyleBackColor = true;
            // 
            // btexcluir
            // 
            this.btexcluir.Location = new System.Drawing.Point(255, 421);
            this.btexcluir.Name = "btexcluir";
            this.btexcluir.Size = new System.Drawing.Size(75, 32);
            this.btexcluir.TabIndex = 9;
            this.btexcluir.Text = "Excluir";
            this.btexcluir.UseVisualStyleBackColor = true;
            // 
            // localizarToolStripMenuItem
            // 
            this.localizarToolStripMenuItem.Name = "localizarToolStripMenuItem";
            this.localizarToolStripMenuItem.Size = new System.Drawing.Size(65, 20);
            this.localizarToolStripMenuItem.Text = "Localizar";
            // 
            // imprimirToolStripMenuItem
            // 
            this.imprimirToolStripMenuItem.Name = "imprimirToolStripMenuItem";
            this.imprimirToolStripMenuItem.Size = new System.Drawing.Size(65, 20);
            this.imprimirToolStripMenuItem.Text = "Imprimir";
            // 
            // observaçõesToolStripMenuItem
            // 
            this.observaçõesToolStripMenuItem.Name = "observaçõesToolStripMenuItem";
            this.observaçõesToolStripMenuItem.Size = new System.Drawing.Size(89, 20);
            this.observaçõesToolStripMenuItem.Text = "Observações ";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.localizarToolStripMenuItem,
            this.imprimirToolStripMenuItem,
            this.observaçõesToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(688, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // Cadrastro_de_Clientes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.ClientSize = new System.Drawing.Size(688, 456);
            this.Controls.Add(this.btexcluir);
            this.Controls.Add(this.btcancelar);
            this.Controls.Add(this.btalterar);
            this.Controls.Add(this.btinserir);
            this.Controls.Add(this.btSalvar);
            this.Controls.Add(this.btfechar);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Cadrastro_de_Clientes";
            this.Text = "Cadrastro_de_Clientes";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.TextBox txtbnome;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.MaskedTextBox mtxtbCPF;
        private System.Windows.Forms.TextBox txtbprofisssao;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.RadioButton radioButton3;
        private System.Windows.Forms.RadioButton radioButton4;
        private System.Windows.Forms.MaskedTextBox mtxtbclientedesde;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.MaskedTextBox mtxtbdatadenacimento;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox txtbcomplemento;
        private System.Windows.Forms.ComboBox cbxestado;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.MaskedTextBox mtxtbCEP;
        private System.Windows.Forms.TextBox txtbnumero;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txbreferencia;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txbcidade;
        private System.Windows.Forms.TextBox txbbairro;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txbrua;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox txbramal;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.MaskedTextBox mtxbtelefonecomercial;
        private System.Windows.Forms.TextBox txbemail;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.MaskedTextBox mtxbcelular;
        private System.Windows.Forms.MaskedTextBox mtxbtelefone;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Button btfechar;
        private System.Windows.Forms.Button btSalvar;
        private System.Windows.Forms.Button btinserir;
        private System.Windows.Forms.Button btalterar;
        private System.Windows.Forms.Button btcancelar;
        private System.Windows.Forms.Button btexcluir;
        private System.Windows.Forms.ToolStripMenuItem localizarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem imprimirToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem observaçõesToolStripMenuItem;
        private System.Windows.Forms.MenuStrip menuStrip1;
    }
}