﻿namespace SIGMA.TelasMenorde40
{
    partial class ObservaçõesdoCliente
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.localizarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.imprimirToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.observaçõesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.mtxbcelular2 = new System.Windows.Forms.MaskedTextBox();
            this.mtxbtelefone2 = new System.Windows.Forms.MaskedTextBox();
            this.btfechar = new System.Windows.Forms.Button();
            this.btSalvar = new System.Windows.Forms.Button();
            this.btexcluir = new System.Windows.Forms.Button();
            this.btcancelar = new System.Windows.Forms.Button();
            this.btalterar = new System.Windows.Forms.Button();
            this.btinserir = new System.Windows.Forms.Button();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 59);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(111, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Anotações de Cliente:";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.localizarToolStripMenuItem,
            this.imprimirToolStripMenuItem,
            this.observaçõesToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(688, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // localizarToolStripMenuItem
            // 
            this.localizarToolStripMenuItem.Name = "localizarToolStripMenuItem";
            this.localizarToolStripMenuItem.Size = new System.Drawing.Size(65, 20);
            this.localizarToolStripMenuItem.Text = "Localizar";
            // 
            // imprimirToolStripMenuItem
            // 
            this.imprimirToolStripMenuItem.Name = "imprimirToolStripMenuItem";
            this.imprimirToolStripMenuItem.Size = new System.Drawing.Size(65, 20);
            this.imprimirToolStripMenuItem.Text = "Imprimir";
            // 
            // observaçõesToolStripMenuItem
            // 
            this.observaçõesToolStripMenuItem.Name = "observaçõesToolStripMenuItem";
            this.observaçõesToolStripMenuItem.Size = new System.Drawing.Size(87, 20);
            this.observaçõesToolStripMenuItem.Text = "Dados Gerais";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(12, 75);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(664, 132);
            this.textBox1.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 270);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(84, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Outros telefones";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(12, 298);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(61, 13);
            this.label17.TabIndex = 4;
            this.label17.Text = "Telefone 2:";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(22, 324);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(51, 13);
            this.label18.TabIndex = 5;
            this.label18.Text = "Celular 2:";
            // 
            // mtxbcelular2
            // 
            this.mtxbcelular2.Location = new System.Drawing.Point(79, 321);
            this.mtxbcelular2.Mask = "(99) 000-0000";
            this.mtxbcelular2.Name = "mtxbcelular2";
            this.mtxbcelular2.Size = new System.Drawing.Size(100, 20);
            this.mtxbcelular2.TabIndex = 6;
            // 
            // mtxbtelefone2
            // 
            this.mtxbtelefone2.Location = new System.Drawing.Point(79, 295);
            this.mtxbtelefone2.Mask = "(99) 000-0000";
            this.mtxbtelefone2.Name = "mtxbtelefone2";
            this.mtxbtelefone2.Size = new System.Drawing.Size(100, 20);
            this.mtxbtelefone2.TabIndex = 7;
            // 
            // btfechar
            // 
            this.btfechar.Location = new System.Drawing.Point(520, 421);
            this.btfechar.Name = "btfechar";
            this.btfechar.Size = new System.Drawing.Size(75, 32);
            this.btfechar.TabIndex = 8;
            this.btfechar.Text = "Fechar";
            this.btfechar.UseVisualStyleBackColor = true;
            // 
            // btSalvar
            // 
            this.btSalvar.Location = new System.Drawing.Point(601, 421);
            this.btSalvar.Name = "btSalvar";
            this.btSalvar.Size = new System.Drawing.Size(75, 32);
            this.btSalvar.TabIndex = 9;
            this.btSalvar.Text = "Salvar";
            this.btSalvar.UseVisualStyleBackColor = true;
            // 
            // btexcluir
            // 
            this.btexcluir.Location = new System.Drawing.Point(255, 421);
            this.btexcluir.Name = "btexcluir";
            this.btexcluir.Size = new System.Drawing.Size(75, 32);
            this.btexcluir.TabIndex = 10;
            this.btexcluir.Text = "Excluir";
            this.btexcluir.UseVisualStyleBackColor = true;
            // 
            // btcancelar
            // 
            this.btcancelar.Location = new System.Drawing.Point(174, 421);
            this.btcancelar.Name = "btcancelar";
            this.btcancelar.Size = new System.Drawing.Size(75, 32);
            this.btcancelar.TabIndex = 11;
            this.btcancelar.Text = "Cancelar";
            this.btcancelar.UseVisualStyleBackColor = true;
            // 
            // btalterar
            // 
            this.btalterar.Location = new System.Drawing.Point(93, 421);
            this.btalterar.Name = "btalterar";
            this.btalterar.Size = new System.Drawing.Size(75, 32);
            this.btalterar.TabIndex = 12;
            this.btalterar.Text = "Alterar";
            this.btalterar.UseVisualStyleBackColor = true;
            // 
            // btinserir
            // 
            this.btinserir.Location = new System.Drawing.Point(12, 421);
            this.btinserir.Name = "btinserir";
            this.btinserir.Size = new System.Drawing.Size(75, 32);
            this.btinserir.TabIndex = 13;
            this.btinserir.Text = "Inserir";
            this.btinserir.UseVisualStyleBackColor = true;
            // 
            // ObservaçõesdoCliente
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.ClientSize = new System.Drawing.Size(688, 456);
            this.Controls.Add(this.btinserir);
            this.Controls.Add(this.btalterar);
            this.Controls.Add(this.btcancelar);
            this.Controls.Add(this.btexcluir);
            this.Controls.Add(this.btSalvar);
            this.Controls.Add(this.btfechar);
            this.Controls.Add(this.mtxbtelefone2);
            this.Controls.Add(this.mtxbcelular2);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.label1);
            this.Name = "ObservaçõesdoCliente";
            this.Text = "ObservaçõesdoCliente";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem localizarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem imprimirToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem observaçõesToolStripMenuItem;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.MaskedTextBox mtxbcelular2;
        private System.Windows.Forms.MaskedTextBox mtxbtelefone2;
        private System.Windows.Forms.Button btfechar;
        private System.Windows.Forms.Button btSalvar;
        private System.Windows.Forms.Button btexcluir;
        private System.Windows.Forms.Button btcancelar;
        private System.Windows.Forms.Button btalterar;
        private System.Windows.Forms.Button btinserir;
    }
}